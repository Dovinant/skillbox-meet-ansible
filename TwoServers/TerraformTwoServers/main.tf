terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-b"
}

# ---- VM1 ----
resource "yandex_compute_instance" "vm-1" {
  name                      = "reactjs-2"
  zone = "ru-central1-a"
  allow_stopping_for_update = true

  resources {
    core_fraction = 20 # Гарантированная доля vCPU
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8kdq6d0p8sij7h5qe3"
    }
  }

  network_interface {
    subnet_id = "e9b1mkd4gslra6kpkp6c"
    nat       = true
  }

  metadata = {
    serial-port-enable = 1
    user-data          = "${file("meta.yml")}"
  }

  scheduling_policy {
    preemptible = true # Делаем VM прерываемой
  }
}

# ---- VM2 ----
resource "yandex_compute_instance" "vm-2" {
  name                      = "reactjs-3"
  zone = "ru-central1-b"
  allow_stopping_for_update = true

  resources {
    core_fraction = 20 # Гарантированная доля vCPU
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8kdq6d0p8sij7h5qe3"
    }
  }

  network_interface {
    subnet_id = "e2lpg0shjj07apvrr2uc"
    nat       = true
  }

  metadata = {
    serial-port-enable = 1
    user-data          = "${file("meta.yml")}"
  }

  scheduling_policy {
    preemptible = true # Делаем VM прерываемой
  }
}

#---- Load Balancer ----
resource "yandex_lb_network_load_balancer" "lb-test" {
  name = "lb-test"

  listener {
    name = "listener-web-servers"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web-servers.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

resource "yandex_lb_target_group" "web-servers" {
  name = "web-servers-target-group"

  target {
    subnet_id = "e9b1mkd4gslra6kpkp6c"
    address   = yandex_compute_instance.vm-1.network_interface.0.ip_address
  }

  target {
    subnet_id = "e2lpg0shjj07apvrr2uc"
    address   = yandex_compute_instance.vm-2.network_interface.0.ip_address
  }
}

#---- Outputs ----
#output "lb_summary" {
#  value = yandex_lb_network_load_balancer.lb-test.*
#}

output "balancer_ip_address" {
  value = "${[for s in yandex_lb_network_load_balancer.lb-test.listener: s.external_address_spec.*.address].0[0]}"
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

